>推荐一些canvas制作的小游戏，提高你的学习兴趣——https://gitee.com/xv700/canvas

## 项目介绍

前端入门到进阶图文教程。从零开始学前端，做一名精致的前端工程师。持续更新中。本项目的主要作用有：  

- 1、网上的大部分入门教程，都不太适合初学者，本项目争取照顾到每一位前端入门者的同理心。即使你完全不懂前端，甚至不懂编程，通过这个教程，也能让小白入门。  

- 2、帮助前端同学提供一个精品学习资源和路线，提高学习效率，少走很多弯路。  
 
- 3、可以当做前端字典，随时翻阅，查漏补缺。  

### 00-前端工具

> 本文更新于 2020-06-19，大家完全不用担心这篇文章会过时，因为随着 VS Code 的版本更新和插件更新，本文也会随之更新。  
> VS Code 软件实在是太酷、太好用了，越来越多的新生代互联网青年正在使用它。   
> 点击阅读：https://gitee.com/xv700/Web/tree/master/00-%E5%89%8D%E7%AB%AF%E5%B7%A5%E5%85%B7

### 01-HTML

> 什么是web，标准是什么，HTML是什么包含什么？   
> 点击阅读：https://gitee.com/xv700/Web/tree/master/01-HTML  

### 02-CSS

> 这是一篇关于CSS文档，能够你从0入门掌握CSS  
> 点击阅读：https://gitee.com/xv700/Web/tree/master/02-CSS

### 03-JavaScript基础

> JavaScript诞生于1995年。布兰登 • 艾奇（Brendan Eich，1961年～），1995年在网景公司，发明的JavaScript。  
> JavaScript是由网景公司发明，起初命名为LiveScript，后来由于SUN公司的介入更名为了JavaScript。  
> 点击阅读：https://gitee.com/xv700/Web/tree/master/03-JavaScript%E5%9F%BA%E7%A1%80

### 04-JavaScript进阶

> 一些高级JavaScript操作  
> 点击阅读：https://gitee.com/xv700/Web/tree/master/04-JavaScript%E8%BF%9B%E9%98%B6


### 05-前端基本功：CSS和DOM练习

> 仿照JD首页进行切图   
> 点击阅读：https://gitee.com/xv700/Web/tree/master/05-%E5%89%8D%E7%AB%AF%E5%9F%BA%E6%9C%AC%E5%8A%9F%EF%BC%9ACSS%E5%92%8CDOM%E7%BB%83%E4%B9%A0

### 05-前端基本功：JavaScript特效

> 一些JavaScript特效   
> 点击阅读：https://gitee.com/xv700/Web/tree/master/05-%E5%89%8D%E7%AB%AF%E5%9F%BA%E6%9C%AC%E5%8A%9F%EF%BC%9AJavaScript%E7%89%B9%E6%95%88

### 06-jQuery

> jQuery 是 js 的一个库，封装了我们开发过程中常用的一些功能，方便我们调用，提高开发效率。  
> 点击阅读：https://gitee.com/xv700/Web/tree/master/06-jQuery

### 07-HTML5和CSS3 

> HTML5并不仅仅只是做为HTML标记语言的一个最新版本，更重要的是它**制定了Web应用开发的一系列标准**，成为第一个将Web做为应用开发平台的HTML语言。  
> 点击阅读：https://gitee.com/xv700/Web/tree/master/07-HTML5%E5%92%8CCSS3

### 08-移动web开发

> Bootstrap 是非常流行的前端框架。特点是：灵活简洁、代码优雅、美观大方。它是由Twitter的两名工程师 Mark Otto 和 Jacob Thornton 在2011年开发的。  
> 点击阅读：https://gitee.com/xv700/Web/tree/master/08-%E7%A7%BB%E5%8A%A8web%E5%BC%80%E5%8F%91

### 09-Ajax

> 在浏览器中，我们可以在不刷新页面的情况下，通过ajax的方式去获取一些新的内容。  
> 点击阅读：https://gitee.com/xv700/Web/blob/master/09-Ajax/02-Ajax%E5%85%A5%E9%97%A8%E5%92%8C%E5%8F%91%E9%80%81http%E8%AF%B7%E6%B1%82.md

### 10-Node.js和模块化

> Node.js 是一个基于 Chrome V8 引擎的 JavaScript 运行环境。 Node.js使用了一个事件驱动、非阻塞式I/O的模型（ Node.js的特性），使其轻量级又高效。      
> 点击阅读：https://gitee.com/xv700/Web/tree/master/10-Node.js%E5%92%8C%E6%A8%A1%E5%9D%97%E5%8C%96  

### 11-ES6

> ECMAScript 是 JS 的语言标准。而 ES6 是新的 JS 语法标准。  
PS：严格来说，ECMAScript 还包括其他很多语言的语言标准。  
> 点击阅读：https://gitee.com/xv700/Web/tree/master/11-ES6 

### 12-Vue基础

> Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。另一方面，当与现代化的工具链以及各种支持类库结合使用时，Vue 也完全能够为复杂的单页应用提供驱动。    
> 点击阅读：https://gitee.com/xv700/Web/tree/master/12-Vue%E5%9F%BA%E7%A1%80  

### 13-React基础

> React 是一个用于构建用户界面的 JAVASCRIPT 库。  
React 主要用于构建UI，很多人认为 React 是 MVC 中的 V（视图）。   
React 起源于 Facebook 的内部项目，用来架设 Instagram 的网站，并于 2013 年 5 月开源。  
React 拥有较高的性能，代码逻辑非常简单，越来越多的人已开始关注和使用它。      
> 点击阅读：https://gitee.com/xv700/Web/tree/master/13-React%E5%9F%BA%E7%A1%80  

### 14-前端面试

> 有备无患,未雨绸缪 
> 点击阅读：https://gitee.com/xv700/Web/tree/master/14-%E5%89%8D%E7%AB%AF%E9%9D%A2%E8%AF%95  

### 15-面试题积累

> 九层之台，起于垒土    
> 点击阅读：https://gitee.com/xv700/Web/tree/master/15-%E9%9D%A2%E8%AF%95%E9%A2%98%E7%A7%AF%E7%B4%AF

### 16-前端进阶

> 前端的几道题目   
> 点击阅读：https://gitee.com/xv700/Web/tree/master/16-%E5%89%8D%E7%AB%AF%E8%BF%9B%E9%98%B6

### 17-前端综合  

> Web前端入门自学路线    
> 点击阅读：https://gitee.com/xv700/Web/tree/master/17-%E5%89%8D%E7%AB%AF%E7%BB%BC%E5%90%88  

### 18-推荐链接      

> 点击阅读：https://gitee.com/xv700/Web/tree/master/18-%E6%8E%A8%E8%8D%90%E9%93%BE%E6%8E%A5

### 19-前端英语      

> 了解学习一些前端相关的英语    
> 点击阅读：https://gitee.com/xv700/Web/tree/master/19-%E5%89%8D%E7%AB%AF%E8%8B%B1%E6%96%87